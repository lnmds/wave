defmodule Wave.DB do
  use GenServer

  def start_link(opts) do
    GenServer.start_link(__MODULE__, :ok, opts)
  end

  def conn(func) do
    dbpid = :global.whereis_name Wave.DB
    func.(dbpid)
  end

  def query(query, args) do
    Wave.DB.conn fn dbpid ->
      Postgrex.query(dbpid, query, args)
    end
  end

  def init(:ok) do
    opts = Application.fetch_env!(:wave, :postgres)
    {:ok, pid} = Postgrex.start_link(opts)
    :global.register_name(Wave.DB, pid)
    {:ok, pid}
  end

end
