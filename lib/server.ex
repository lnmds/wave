defmodule Wave.Service do
  use Ace.HTTP.Service, [port: 8080, cleartext: true]

  @impl Raxx.Server
  def handle_request(%{method: :GET, path: []}, _state) do
    response(:ok)
    |> set_header("content-type", "text/plain")
    |> set_body("hi bitch")
  end

  def handle_request(%{method: :POST, path: ["api", "login"], body: body}, _state) do
    json = Poison.decode!(body)

    user = json["user"]
    password = json["password"]

    {r, user_id, hash} = Wave.User.check_password(user, password)

    if r do
      token = Wave.Token.sign(:nontimed, user_id |> to_string, hash)
      response(:ok)
      |> set_header("content-type", "text-json")
      |> set_body(Poison.encode!(%{token: token}))
    else
      response(:ok)
      |> set_header("content-type", "text-json")
      |> set_body(Poison.encode!(%{error: true, value: "invalid password"}))
    end
  end
end
