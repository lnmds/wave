defmodule Wave.Starter do
  use GenServer
  require Logger

  def start_link(opts) do
    GenServer.start_link(__MODULE__, :ok, opts)
  end

  def init(:ok) do
    admin = Application.fetch_env!(:wave, :admin_user)

    case Wave.User.add_user(admin.username, admin.password) do
      :ok -> Logger.info "Created admin user."
      {:error, :user_exists} -> Logger.info "Already created admin user"
      {:error, err} -> exit(err)
    end

    {:ok, nil}
  end
end
