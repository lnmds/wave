defmodule Wave do
  use Application

  require Logger

  def start(_type, _args) do
    children = [
      {Wave.DB, []},
      {Snowflake, [
        worker_id: 1, 
      ]},
      {Wave.Service, []},
      {Wave.Starter, []},
    ]

    opts = [strategy: :one_for_one, name: Wave.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
